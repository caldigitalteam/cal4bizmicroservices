using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationManager.Models
{
    public class LoginRequest
    {

        public string Username { get; set; }

        public string Password { get; set; }

        public bool IsRegisterToSwipe { get; set; }

        public string UUID { get; set; }

    }
    public class LoginResponse
    {
        public Auth Auth { get; set; }

    }

    public class Auth
    {
        public string SwipeGuid { get; set; }

        public string Token { get; set; }
    }
}
