﻿using API.Controllers.Credits;
using API.Controllers.MetaData;
using API.Engine;
using GeneralUtils.HealthCheck;
using LoggerService;
using Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RedisService;
using StackExchange.Redis.Extensions.Core.Configuration;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddHttpClient<CreditsManagerProxy>();

            services.AddSingleton<IRedis, RedisClient>();
            
            services.AddHttpClient<IMetaDataProxy, MetaDataProxy>();

            services.AddLogging("Cal4Biz");

            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddMvcOptions(option => option.Filters.Add(new LogFilterAttribute()));

            services.AddHealthChecks()
                .AddCalUrl(Configuration.GetSection("Dependencies").Value);

            ConfigureDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, LoggingSink loggingSink)
        {
            AuditProvider auditProvider = new AuditProvider(loggingSink);
            loggerFactory.AddProvider(auditProvider);            
            app.UseMvc();
        }
    }
}
