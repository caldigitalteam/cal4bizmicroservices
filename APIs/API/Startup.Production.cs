﻿using Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace API
{
    public class StartupProduction : Startup
    {
        public StartupProduction(IConfiguration host, IHostingEnvironment env) : base(host, env)
        {
        }

        public override void ConfigureServices(IServiceCollection services)
        {

            base.ConfigureServices(services);
        }

        public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, LoggingSink loggingSink)
        {
           
            base.Configure(app, env, loggerFactory, loggingSink);
        }

    }
}
