﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Model;
using Microsoft.AspNetCore.Mvc;

namespace API.Engine
{
    public class ApiHandler : ControllerBase
    {
        public async Task<ActionResult<DataResponse<T>>> CreateResponse<T>(T response, int methodeId, char severityCode, int severityNumber)
        {
            var baseResponse = await GetMessages(methodeId, severityCode, severityNumber);

            var apiResponse = new DataResponse<T>(response, baseResponse);

            return Ok(apiResponse);
        }

        private async Task<Response> GetMessages(int methodeId, char severityCode, int severityNumber)
        {
            return await Task.Run(() =>
            {
                Task.Delay(0);
                return new Response();
            });
                
        }
    }
}
