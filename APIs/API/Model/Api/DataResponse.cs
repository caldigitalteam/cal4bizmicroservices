﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class DataResponse<T> : Response
    {      
        public DataResponse(T data , Response baseResponse)
            : base(baseResponse)
        {
            Data = data;
        }

        public T Data { get; set; }
    }
}
