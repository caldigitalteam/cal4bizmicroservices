﻿
using System.Threading.Tasks;

namespace API.Model
{
    public class Response
    {
        public Response()
        {

        }
        public Response(Response response)
        {
            Custom = response.Custom;
            DialogBox = response.DialogBox;
            CancelButtonText = response.CancelButtonText;
            Content = response.Content;
            OkButtonText = response.OkButtonText;
            Orientation = response.Orientation;
            Title = response.Title;
            Status = new Status
            {
                Description = response.Status.Description,
                Message = response.Status.Message,
                ReturnUrl = response.Status.ReturnUrl,
                SeverityCode = response.Status.SeverityCode,
                SeverityNumber = response.Status.SeverityNumber,
                Succeeded = response.Status.Succeeded
            };
        }

        public string Custom { get; set; }
        public string DialogBox { get; set; }
        public string CancelButtonText { get; set; }
        public string Content { get; set; }
        public string OkButtonText { get; set; }
        public string Orientation { get; set; }
        public string Title { get; set; }
        public Status Status { get; set; }
    }

    public class Status
    {
        public string Description { get; set; }
        public string Message { get; set; }
        public string ReturnUrl { get; set; }
        public string SeverityCode { get; set; }
        public string SeverityNumber { get; set; }
        public bool Succeeded { get; set; }
    }
}
