﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers.Credits
{
    public class GetCreditsDetailsResponse
    {
        public string Count { get; set; }

        public List<Credit> Credits { get; set; }

        public class Bpm676Response
        {
            public string Count { get; set; }

            public List<Credit> Credits { get; set; }
        }

        public class Credit
        {

            public int Code { get; set; }


            public List<CreditCompanyItem> CreditCompanies { get; set; }
        }

        public class CreditCompanyItem
        {

            public int Code { get; set; } // 0 = כל המותגים, 1= ויזה , 3= דיינרס , 4= ישראכרט, 6= מסטרכרד


            public string FutureBalanceAmount { get; set; }


            public NextCredit NextCredit { get; set; }


            public List<MonthlyCreditItem> MonthlyCredits { get; set; }

        }

        public class NextCredit
        {

            public string NextCreditDate { get; set; }


            public string NextNetAmount { get; set; }
        }

        public class MonthlyCreditItem
        {

            public string DateMM { get; set; }


            public string DateYY { get; set; }


            public string MonthName { get; set; }


            public string NextNetAmount { get; set; }
        }

    }
}
