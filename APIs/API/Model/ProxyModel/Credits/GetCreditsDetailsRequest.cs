﻿using API.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers.Credits
{
    public class GetCreditsDetailsRequest
    {
        [RegularExpression(Regexes.DateMMSleshYYYY)]
        public string From { get; set; }

        [RegularExpression(Regexes.DateMMSleshYYYY)]
        public string To { get; set; }

        //[RegularExpression(@"^\d+$")]
        public string[] Products { get; set; }
    }
}
