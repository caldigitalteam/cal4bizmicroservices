﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers.Deposits
{
    public class GetDepositsDetailsResponse
    {
        public class Response
        {
            
            public string Count { get; set; }
            
            public List<Deposit> Deposits { get; set; }
        }

        public class Bpm677Response
        {
            
            public string Count { get; set; }
            
            public List<Deposit> Deposits { get; set; }
        }

        public class Deposit
        {
            
            public string Code { get; set; }

            
            public List<CreditCompany> CreditCompanies { get; set; }
        }

        public class CreditCompany
        {
            
            public string Code { get; set; }

            
            public List<MonthlyDeposit> MonthlyDeposits { get; set; }
        }

        public class MonthlyDeposit
        {
            
            public string DateMM { get; set; }

            
            public string DateYY { get; set; }

            
            public string MonthName { get; set; }

            
            public string Sum { get; set; }
        }

    }
}
