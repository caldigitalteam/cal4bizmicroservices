﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Authentication;
using API.Model.ProxyModel.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisService;

namespace API.Controllers.Credits
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IAuthenticationProxy _authenticationProxy;

        public AuthenticationController(ILogger<AuthenticationController> logger, IAuthenticationProxy authenticationProxy)
        {
            _logger = logger;
            _authenticationProxy = authenticationProxy;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<LoginResponse>> Login([FromBody] LoginRequest request)
        {
            var res = await _authenticationProxy.Login(request, _logger);

            return res;
        }
    }
}
