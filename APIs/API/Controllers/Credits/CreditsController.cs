﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisService;

namespace API.Controllers.Credits
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditsController : ControllerBase
    {
        private readonly ILogger<CreditsController> _logger;
        private readonly CreditsManagerProxy _creditsManagerProxy;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRedis _redis;

        public CreditsController(ILogger<CreditsController> logger, CreditsManagerProxy creditsManagerProxy, IHttpContextAccessor httpContextAccessor, IRedis redis)
        {
            _logger = logger;
            _creditsManagerProxy = creditsManagerProxy;
            _httpContextAccessor = httpContextAccessor;
            _redis = redis;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<GetCreditsDetailsResponse>> GetCreditDetails([FromBody] GetCreditsDetailsRequest request)
        {
            //validation
            var add = await _redis.Add<string>("r", "1", 5);
            var get = await _redis.Get<string>("r");
            var delete = await _redis.Delete("r");

            var trace = HttpContext.TraceIdentifier;
            _logger.Log(LogLevel.Trace, "1111111");
            _logger.LogInformation("222222");            

            var res = await _creditsManagerProxy.GetCreditDetailsAsync(request, _logger);

            return res;
        }
    }
}
