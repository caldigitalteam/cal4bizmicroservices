﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers.Deposits
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepositsController : ControllerBase
    {
        private readonly ILogger<DepositsController> _logger;
        private readonly DepositsManagerProxy _depositsManagerProxy;

        public DepositsController(ILogger<DepositsController> logger, DepositsManagerProxy depositsManagerProxy)
        {
            _logger = logger;
            _depositsManagerProxy = depositsManagerProxy;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<GetDepositsDetailsResponse>> GetCreditDetails([FromBody] GetDepositsDetailsRequest request)
        {
            //validation
            _logger.LogInformation("API.GetDepositsDetails start request", request);

            var res = await _depositsManagerProxy.GetDepositDetails(request);

            //messeges     

            return res;
        }
    }
}
