﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Engine;
using API.Model;
using GeneralUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers.MetaData
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetaDataController : ApiHandler
    {
        private readonly ILogger<MetaDataController> _logger;
        private readonly IMetaDataProxy _metaDataProxy;

        public MetaDataController(ILogger<MetaDataController> logger, IMetaDataProxy metaDataProxy)
        {
            _logger = logger;
            _metaDataProxy = metaDataProxy;
        }

        [HttpGet]        
        public async Task<ActionResult<DataResponse<MetaDataResponse>>> GetMetaData([FromQuery] MetaDataRequest request)
        {

            MetaDataResponse response = null;

            if (!ModelState.IsValid)
            {
                return await CreateResponse<MetaDataResponse>(null, 1, SeverityStatus.Warning, 1);                
            }
            else
            {
                try
                {
                    response = await _metaDataProxy.GetMetaDataDetails(request, _logger);

                    if (response == null)
                    {
                        throw new Cal4BizException(1, SeverityStatus.Warning, 1);
                    }
                }
                catch (Cal4BizException bizEx)
                {
                    _logger.LogError(bizEx.Message);
                    return await CreateResponse<MetaDataResponse>(null, 1, bizEx.SeverityCode, bizEx.SeverityNumber);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return await CreateResponse<MetaDataResponse>(null, 1, SeverityStatus.Error, 1);
                }
            }

            return await CreateResponse<MetaDataResponse>(response, 1, SeverityStatus.Success, 1);
        }
    }
}
