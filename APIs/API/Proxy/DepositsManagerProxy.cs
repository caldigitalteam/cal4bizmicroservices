﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RESTService;

namespace API.Controllers.Deposits
{
    public interface IDepositsManagerProxy
    {
        Task<GetDepositsDetailsResponse> GetDepositDetails(GetDepositsDetailsRequest request);
    }

    public class DepositsManagerProxy
    {
        private readonly HttpClient _client;

        public DepositsManagerProxy(HttpClient client, ILogger<DepositsController> logger, IConfiguration configuration)
        {
            _client = client;
            _client.BaseAddress = new Uri(configuration.GetSection("APIProxies:DepositsManager").Value);
        }

        internal Task<GetDepositsDetailsResponse> GetDepositDetails(GetDepositsDetailsRequest request)
        {
            return RestService.GetRestJsonResponse<GetDepositsDetailsRequest, GetDepositsDetailsResponse>(_client, "Api/Deposits", RESTService.RestService.eHttpMethod.HttpPost, request);
        }
    }
}
