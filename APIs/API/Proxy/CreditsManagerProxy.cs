﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
//using Logging;
using Microsoft.Extensions.Logging;
using RESTService;

namespace API.Controllers.Credits
{
    public interface IDepositsManagerProxy
    {
        Task<GetCreditsDetailsResponse> GetCreditDetailsAsync(GetCreditsDetailsRequest request, ILogger<CreditsController> _logger);
    }

    public class CreditsManagerProxy
    {
        private readonly HttpClient _client;
        private readonly ILogger<CreditsController> _logger;

        public CreditsManagerProxy(HttpClient client, ILogger<CreditsController> logger, IConfiguration configuration)
        {
            _client = client;
            _logger = logger;
            _client.BaseAddress = new Uri(configuration.GetSection("APIProxies:CreditsManager").Value);            
        }

        internal async Task<GetCreditsDetailsResponse> GetCreditDetailsAsync(GetCreditsDetailsRequest request, ILogger<CreditsController> _logger)
        {
            return await RestService.GetRestJsonResponse<GetCreditsDetailsRequest, GetCreditsDetailsResponse>(_client, "Api/Credits", RESTService.RestService.eHttpMethod.HttpPost, request);
        }
    }
}
