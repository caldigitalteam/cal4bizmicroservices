﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GeneralUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RESTService;
using API.Model.ProxyModel.Authentication;
using API.Controllers.Credits;

namespace API.Controllers.Authentication
{
    public interface IAuthenticationProxy
    {
        Task<LoginResponse> Login(LoginRequest request, ILogger<AuthenticationController> _logger);
    }

    public class AuthenticationProxy : IAuthenticationProxy
    {
        private readonly HttpClient _client;
        private readonly ILogger<AuthenticationController> _logger;

        public AuthenticationProxy(HttpClient client, ILogger<AuthenticationController> logger, IConfiguration configuration)
        {
            _client = client;
            _logger = logger;            
            _client.BaseAddress = new Uri(configuration.GetSection("APIProxies:AuthenticationManager").Value);
        }

        public Task<LoginResponse> Login(LoginRequest request, ILogger<AuthenticationController> _logger)
        { 
            return RestService.GetRestJsonResponse<LoginRequest, LoginResponse>(_client, "Api/Login", RestService.eHttpMethod.HttpGet);
        }
    }
}
