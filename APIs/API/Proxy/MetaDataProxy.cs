﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GeneralUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RESTService;

namespace API.Controllers.MetaData
{
    public interface IMetaDataProxy
    {
        Task<MetaDataResponse> GetMetaDataDetails(MetaDataRequest request, ILogger<MetaDataController> _logger);
    }

    public class MetaDataProxy : IMetaDataProxy
    {
        private readonly HttpClient _client;
        private readonly ILogger<MetaDataController> _logger;

        public MetaDataProxy(HttpClient client, ILogger<MetaDataController> logger, IConfiguration configuration)
        {
            _client = client;
            _logger = logger;            
            _client.BaseAddress = new Uri(configuration.GetSection("APIProxies:MetaDataManager").Value);
        }

        public Task<MetaDataResponse> GetMetaDataDetails(MetaDataRequest request, ILogger<MetaDataController> _logger)
        {
            string queryString = request.QueryString();
            string path2Api = $"api/MetaData/GetMetaData?{queryString}";

            return RestService.GetRestJsonResponse<MetaDataRequest, MetaDataResponse>(_client, path2Api, RESTService.RestService.eHttpMethod.HttpGet);
        }
    }
}
