﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cal.Infrastructure.AspNetServices.Validation
{
    // See: https://msdn.microsoft.com/magazine/mt767699?f=255&MSPPError=-2147217396
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string GetErrorMessages(ModelStateEntry entry) =>
                string.Join("; ", entry.Errors.Select(error => error.ErrorMessage));

            if (!context.ModelState.IsValid)
            {
                string valueName;
                string[] messages;

                var errorneousValues = context.ModelState
                    .Where(modelEntry => modelEntry.Value.Errors.Count > 0)
                    .ToList();

                if (errorneousValues.Count > 1)
                {
                    valueName = "Multiple errorneous values";
                    messages = errorneousValues
                        .Select(modelEntry => $"Argument={modelEntry.Key}, Errors={GetErrorMessages(modelEntry.Value)}")
                        .ToArray();
                }
                else
                {
                    var errorneousValue = errorneousValues.Single();
                    valueName = errorneousValue.Key;
                    messages = new[] { GetErrorMessages(errorneousValue.Value) };
                }

                context.Result = InvalidModel((Controller)context.Controller, valueName, messages);
            }
        }

        private static IActionResult InvalidModel(Controller controller, string valueName, params string[] messages) =>
            controller.BadRequest(
                new 
                {
                    ErrorCode = "InvalidModel",
                    Body = valueName,
                    Messages = messages ?? new[] { "The specified value is invalid." }
                });
    }
}
