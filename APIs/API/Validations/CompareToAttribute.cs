﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cal.Infrastructure.AspNetServices.Validation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class CompareToAttribute : ValidationAttribute
    {
        public CompareToAttribute(string otherProperty, ComparisonType comparisonType)
        {
            OtherProperty = otherProperty ?? throw new ArgumentNullException(nameof(otherProperty));
            ComparisonType = comparisonType;
        }

        public string OtherProperty { get; }

        public ComparisonType ComparisonType { get; }

        // See http://stackoverflow.com/a/1365669
        public override object TypeId => this;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var otherProperty = GetOtherValue(validationContext.ObjectInstance);

            Type thisPropertyType = GetThisPropertyType(validationContext);

            if (!thisPropertyType.Equals(otherProperty.Type))
            {
                throw new InvalidOperationException($"Can't compare {thisPropertyType.Name} to {otherProperty.Type.Name}.");
            }

            if (!IsIComparable(thisPropertyType))
            {
                throw new InvalidOperationException($"Type {thisPropertyType.Name} must implement IComparable<{thisPropertyType.Name}>.");
            }

            if (value is null)
            {
                return BothValuesAreNull(otherProperty.Value);
            }

            if (!IsConditionMet(thisPropertyType, value, otherProperty.Value))
            {
                return new ValidationResult($"The value of {validationContext.MemberName} is not {ComparisonType} that of {OtherProperty}.");
            }

            return ValidationResult.Success;
        }

        private bool IsConditionMet(Type type, object left, object right)
        {
            int comparisonResult = Compare(type, left, right);

            switch (ComparisonType)
            {
                case ComparisonType.BiggerThan:
                    return comparisonResult > 0;
                case ComparisonType.EqualTo:
                    return comparisonResult == 0;
                case ComparisonType.LessThan:
                    return comparisonResult < 0;
                case ComparisonType.BiggerThanOrEqualTo:
                    return comparisonResult >= 0;
                case ComparisonType.LessThanOrEqualTo:
                    return comparisonResult <= 0;
                default:
                    throw new InvalidOperationException("Unexpecte comparison type: " + ComparisonType);
            }
        }

        private static int Compare(Type type, object left, object right)
        {
            var comparableType = typeof(IComparable<>).MakeGenericType(type);
            var compareMethod = comparableType.GetMethod(nameof(IComparable<object>.CompareTo));
            object result = compareMethod.Invoke(left, new[] { right });
            return (int)result;
        }

        private static bool IsIComparable(Type type) =>
            typeof(IComparable<>).MakeGenericType(type).IsAssignableFrom(type);

        private static Type GetThisPropertyType(ValidationContext validationContext)
        {
            return validationContext.ObjectInstance.GetType().GetProperty(validationContext.MemberName).PropertyType;
        }

        private ValidationResult BothValuesAreNull(object otherValue)
        {
            if (ComparisonType == ComparisonType.EqualTo)
            {
                if (otherValue is null)
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult("Values are not equal.");
            }
            else
            {
                return new ValidationResult($"Can't use the {ComparisonType} operator with a null value.");
            }
        }

        private (Type Type, object Value) GetOtherValue(object objectInstance)
        {
            var property = objectInstance.GetType().GetProperty(OtherProperty);

            if (property is null)
            {
                throw new ArgumentException($"Property {OtherProperty} doesn't exist in type {objectInstance.GetType().Name}.");
            }

            return (property.PropertyType, property.GetValue(objectInstance));
        }
    }

    public enum ComparisonType
    {
        BiggerThan,
        EqualTo,
        LessThan,
        BiggerThanOrEqualTo,
        LessThanOrEqualTo,
    }
}
