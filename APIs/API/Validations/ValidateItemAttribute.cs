﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Cal.Infrastructure.AspNetServices.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ValidateItemAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is IEnumerable enumerable)
            {
                int index = 0;

                var results = new List<ValidationResult>();

                foreach (var item in enumerable)
                {
                    if (!Validator.TryValidateObject(item, new ValidationContext(item), results))
                    {
                        var firstError = results.First();

                        var result = new ValidationResult(
                            $"Item[{index}] is invalid: {firstError.ErrorMessage}",
                            firstError.MemberNames.Select(propertyName => $"Item[{index}].{propertyName}"));

                        return firstError;
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
