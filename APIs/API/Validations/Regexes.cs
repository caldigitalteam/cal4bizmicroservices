﻿

namespace API.Validations
{
    public class Regexes
    {
        public const string OnlyDigits = @"^[0-9]*$";

        public const string DigitsOrEmpty = @"^(\s*|\d+)$";

        public const string DigitsFormat = @"^([0-9]{{0},{1}})$";

        public const string Digits1 = @"(^$)|(^\d{1}$)";

        public const string DigitsMax2 = @"(^$)|(^\d{1,2}$)";

        public const string Digits3 = @"(^$)|(^\d{3}$)";

        public const string DigitsMax3 = @"(^$)|(^\d{1,3}$)";

        public const string Digits4 = @"(^$)|(^\d{4}$)";

        public const string DigitsMax4 = @"(^$)|(^\d{1,4}$)";

        public const string DigitsMax6 = @"(^$)|(^\d{1,6}$)";

        public const string DigitsMax8 = @"(^$)|(^\d{1,8}$)";

        public const string Digits7 = @"(^$)|(^\d{7}$)";

        public const string Digits8 = @"(^$)|(^\d{8}$)";

        public const string Digits9 = @"(^$)|(^\d{9}$)";

        public const string DigitsMax9 = @"(^$)|(^\d{1,9}$)";

        public const string DigitsMax12 = @"(^$)|(^\d{1,12}$)";

        public const string Digits12 = @"(^$)|(^\d{12}$)";

        public const string Digits15 = @"(^$)|(^\d{15}$)";

        public const string DigitsMax15 = @"(^$)|(^\d{1,15}$)";

        public const string Digits16 = @"(^$)|(^\d{16}$)";

        public const string DigitsMax16 = @"(^$)|(^\d{1,16}$)";

        public const string Digits20 = @"(^$)|(^\d{20}$)";

        public const string DigitsMax20 = @"(^$)|(^\d{1,20}$)";

        public const string AnyCharMax1 = @"^.{0,1}";

        public const string AnyCharMax2 = @"^.{0,2}";

        public const string AnyCharMax8 = @"^.{0,8}";

        public const string AnyCharMax20 = @"^.{0,20}";

        public const string AnyCharMax30 = @"^.{0,30}";

        public const string AnyCharMax50 = @"^.{0,50}";

        public const string AnyCharMax800 = @"^.{0,800}";

        public const string Amount2 = @"^\s*(?=.*[0-9])\d*(?:\.\d{0,9})?\s*$"; // OK: 123, 123.0, 123.00

        public const string Amount = @"^\d+(\.\d{1,2})?$"; // OK: 123, 123.0, 123.00

        public const string AmountNullable = @"(^$)|(^[0-9\.]+$)";

        public const string DateMMSleshYYYY = @"^\d{2}\/\d{4}$";

        public const string LengthFormat = @"^.{{0},{1}}";

        public const string PhoneNumber = @"^([0-9]{9,10})$";

        public const string UUID = @"^[a-z,A-Z,0-9\=\-]+$";

        public const string Username = @"^([a-z,A-Z,0-9]{8})$";

        public const string Password = @"^([a-z,A-Z,0-9]{8,12})$";

        public const string PasswordAtLeastOneFromEach = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";

        public const string Sha512Hash = @"^.{0,200}"; //@"^[a-z,A-Z,0-9\-\=\+\\]+$";

        public const string Email = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        public const string DateOrEmpty = @"^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})?$";

        public const string Version = @"^\d+(\.\d+)*$"; // digits and dots

        public const string Token = @"^[a-z,A-Z,0-9\=]+$";

        public const string BoolChar = @"^([0,1]{1})$";

        public static string AnyChar(int min, int max)
        {
            return string.Format(LengthFormat, min.ToString(), max.ToString());
        }

        public static string Digits(int min, int max)
        {
            return string.Format(DigitsFormat, min.ToString(), max.ToString());
        }
    }
}
