﻿using System;

namespace Cal.Infrastructure.AspNetServices.Validation
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class ChoiceAttribute : Attribute
    {
        public ChoiceAttribute(string choiceName = null)
        {
            ChoiceName = choiceName;
        }

        public string ChoiceName { get; }
    }
}
