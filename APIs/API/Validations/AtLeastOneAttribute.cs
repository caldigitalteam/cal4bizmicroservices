﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Cal.Infrastructure.AspNetServices.Validation
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class AtLeastOneAttribute : ValidationAttribute
    {
        public AtLeastOneAttribute(string choiceName = null)
        {
            ChoiceName = choiceName;
        }

        public string ChoiceName { get; }

        // See http://stackoverflow.com/a/1365669
        public override object TypeId => this;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is null)
            {
                return ValidationResult.Success;
            }

            var choiceProperties =
                (from property in value.GetType().GetProperties()
                 let choiceAttr = (ChoiceAttribute)property.GetCustomAttributes(typeof(ChoiceAttribute), false).FirstOrDefault()
                 where choiceAttr != null && choiceAttr.ChoiceName == ChoiceName
                 let propertyValue = property.GetValue(value)
                 select (property.Name, Value: propertyValue))
                .ToArray();

            bool atLeastOneSpecified = choiceProperties.Any(property => property.Value != null);

            if (!atLeastOneSpecified)
            {
                var properties = string.Join(", ", choiceProperties.Select(property => property.Name));
                return new ValidationResult($"At least one of the properties ({properties})must be specified must be specified for choice {ChoiceName ?? "(default)"}.");
            }

            return ValidationResult.Success;
        }
    }
}
