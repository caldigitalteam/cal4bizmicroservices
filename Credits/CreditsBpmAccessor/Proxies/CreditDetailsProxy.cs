using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditsBpmAccessor.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RESTService;

namespace CreditsBpmAccessor.Proxies
{
    public interface ICreditDetailsProxy
    {
        Task<GetCreditsDetails.Bpm676Response> GetCreditDetails(GetCreditsDetails.Request request);
    }

    public class CreditDetailsProxy : ICreditDetailsProxy
    {
        private readonly HttpClient _client;

        public CreditDetailsProxy(HttpClient client, IOptionsSnapshot<ProxySettings> proxySettings)
        {
            _client = client;
            _client.BaseAddress = proxySettings.Get("Bpm676Url").Uri;
        }

        public async Task<GetCreditsDetails.Bpm676Response> GetCreditDetails(GetCreditsDetails.Request request)
        {
            GetCreditsDetails.Bpm676Response response = null; //await RestService.GetRestJsonResponse<GetCreditsDetails.Request, Bpm676>(_client, "api/credits", RestService.eHttpMethod.HttpPost, request);

            string mock = await System.IO.File.ReadAllTextAsync(".\\Mocks\\676_RES.json");
            response = JsonConvert.DeserializeObject<GetCreditsDetails.Bpm676Response>(mock);

            return response;
        }
    }
}
