
using System.Collections.Generic;
using CreditsBpmAccessor.Models.CreditLine;

namespace CreditsBpmAccessor.Models
{
    
    
    public partial class GetCreditsByHash
    {
        public class Response
        {
            
            public QuickView QuickView { get; set; }
        }

        public class QuickView
        {
            
            public string SwipeGuid { get; set; }

            
            public string Count { get; set; }
            
            public List<CreditsPerCurrency> CreditsPerCurrency { get; set; }

            public CreditLineEntity.CreditLineObligo.Response CreditLineObligo { get; set; }
        }
        
        public class CreditsPerCurrency
        {
            
            public int Code { get; set; } // 0 = כל המותגים, 1= ויזה , 3= דיינרס , 4= ישראכרט, 6= מסטרכרד

            
            public string NextCreditDate { get; set; }

            
            public string NextNetAmount { get; set; }

            
            public string FutureBalanceAmount { get; set; }
        }
        
    }
}
