
using System.Collections.Generic;


namespace CreditsBpmAccessor.Models
{
    public partial class Bpm676CreditsDetails
    {

        public class Response
        {
            public string Count { get; set; }
            public List<CurrencyItem> CurrencyArray { get; set; }
        }

        public class CurrencyItem
        {
            public string CurrencyCode { get; set; }

            public List<CreditCompanyItem> CreditCompanyArray { get; set; }
        }

        public class CreditCompanyItem
        {
            public int CreditCompanyCode { get; set; } // 0 = כל המותגים, 1= ויזה , 3= דיינרס , 4= ישראכרט, 6= מסטרכרד

            public string FutureTrnBalanceAmt { get; set; }

            public NextCredit NextCredit { get; set; }

            public List<MonthlyCreditItem> MonthlyCreditArray { get; set; }

        }

        public class NextCredit
        {
            public string NextDebCrdDate { get; set; }
            public string NextTotXferAmt { get; set; }
        }

        public class MonthlyCreditItem
        {
            public string DebCrdDateMM { get; set; }

            public string DebCrdDateYY { get; set; }

            public string TotXferAmt { get; set; }
        }

    }
}
