
using System.Collections.Generic;


namespace CreditsBpmAccessor.Models
{

    public partial class CreditLineObligo
    {
        public class Response
        {
            
            public Obligo Obligo { get; set; }
        }

        public class Obligo
        {
            
            public int MerchArrayCounter { get; set; }

            
            public decimal CheckedObligoSum { get; set; }

            
            public string MinAmtForTrn { get; set; }

            
            public string InterestDate { get; set; }

            
            public List<MerchantObligo> MerchantObligo { get; set; }
        }

        public class MerchantObligo
        {
            
            public string StatusId { get; set; }

            
            public string ExtIdNumTypeCode { get; set; }

            
            public string ProdExtId { get; set; }

            
            //[IgnoreDataMember]
            public decimal DecimalMaxAmtForTrn { get; set; }

            
            public int MaxAmtForTrn { get; set; }

            
            public string InterestPrecent { get; set; }

            
            public string InterestType { get; set; }

            
            public string InterestDesc { get; set; }

            
            public string ActualInterestPrecent { get; set; }

            
            public string CommisSum { get; set; }

            
            public string CommisDesc { get; set; }
        }
    }
}
