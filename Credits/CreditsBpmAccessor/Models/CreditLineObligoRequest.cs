using System.Collections.Generic;


namespace CreditsBpmAccessor.Models
{
    public partial class CreditLineObligo
    {

        public class Request
        {
            public int ApplicationSource { get; set; }

            public int MerchArrayCounter { get; set; }

            public string BuAccUserName { get; set; }

           
            public string CheckInd { get; set; }

            
            public List<Merchant> Merchants { get; set; }
        }

        public class Merchant
        {
            
            public int ExtIdNumTypeCode { get; set; }

           
            public string ProdExtId { get; set; }
        }
    }
}
