using System.Collections.Generic;


namespace CreditsBpmAccessor.Models
{
    
    public partial class CreditLineRequest
    {
        public class Response
        {
            
            public Obligo Obligo { get; set; }
        } 

        public class Obligo
        {
            
            public int MerchArrayCounter { get; set; }

            
            public float CheckedObligoSum { get; set; }

            
            public float MinAmtForTrn { get; set; }

            
            public string InterestDate { get; set; }

            
            public string SRNumber { get; set; }

            
            public decimal DecimalTotalTrnAmount { get; set; }

            
            public decimal DecimalTotalTrnAmountDenied { get; set; }

            
            public int TotalTrnAmount { get; set; }

            
            public int TotalTrnAmountDenied { get; set; }

            
            public List<MerchantObligo> MerchantObligo { get; set; }

            public Management Management { get; set; }
        }

        public class Management
        {
            public string First_Group_Flag { get; set; }

            public string Next_Group_Flag { get; set; }

            public string Group_Number { get; set; }

            public string Group_NumOfLines { get; set; }

            public string Function { get; set; }

            public string State_ID { get; set; }

            public string First_Pacc_Flag { get; set; }

            public string Next_Pacc_Flag { get; set; }
        }

        public class MerchantObligo
        {
            
            public bool IsObligoRequestApprove { get; set; }
            
            public string ExtIdNumTypeCode { get; set; }
            
            public string ProdExtId { get; set; }
            
            public string MaxAmtForTrn { get; set; }
            
            public decimal DecimalMaxAmtForTrn { get; set; }
            
            public string TrnAmt { get; set; }
            
            public string InterestPrecent { get; set; }
            
            public string InterestType { get; set; }
            
            public string InterestDesc { get; set; }
            
            public string ActualInterestPrecent { get; set; }
            
            public string CommisSum { get; set; }
            
            public string CommisDesc { get; set; }
            
            public string AccBankNum { get; set; }
            
            public string AccBranchNum { get; set; }
            
            public string AccTypeCode { get; set; }
            
            public string AccNum { get; set; }
            
            public string StatusId { get; set; }
            
            public string StatusDesc { get; set; }
        }
    }
}
