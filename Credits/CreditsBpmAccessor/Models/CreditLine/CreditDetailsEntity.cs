
namespace CreditsBpmAccessor.Models.CreditLine
{
    public partial class CreditDetailsEntity
    {
        //[TypeConverter(typeof(EnumNumericConverter))]
        public enum eApplicationSource
        {
            BusinessSite = 35,
            BusinessApp = 36
        }

        public partial class GetCreditsDetails
        {
            #region Request
            
            public class Request
            {
                
                public string FromDebCrdDate { get; set; }

                
                public string ToDebCrdDate { get; set; }

                
                public string[] ProdExtId { get; set; }
            }
            #endregion

            #region Response
            
            public class Response
            {
                
                public string pCount { get; set; }

                
                public CurrencyItem[] CurrencyArray { get; set; }
            }

           
            public class CurrencyItem
            {
                public int CurrencyCode { get; set; }

                public CreditCompanyItemType[] CreditCompanyArray { get; set; }
            }

            public class CreditCompanyItemType
            {
                public int CreditCompanyCode { get; set; }

                public string FutureTrnBalanceAmount { get; set; }

                
                public NextCreditType NextCredit { get; set; }
                
                
                public MonthlyCreditItem[] MonthlyCreditArray { get; set; }
            }

            
            public class NextCreditType
            {
                
                public string NextDebitCreditDate { get; set; }
                
                public int NextCreditNetAmount { get; set; }
            }

            
            public class MonthlyCreditItem
            {
                
                public string DebitCreditDateMM { get; set; }
                
                public string DebitCreditDateYY { get; set; }
                
                public string NextCreditNetAmount { get; set; }
            }
            #endregion
        }
    }

}
