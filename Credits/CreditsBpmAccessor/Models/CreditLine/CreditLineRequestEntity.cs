using System.Collections.Generic;
using Newtonsoft.Json;

namespace CreditsBpmAccessor.Models.CreditLine
{
    
    public partial class CreditLineEntity
    {
        public partial class CreditLineRequest
        {
            #region Request
            
            public class Request
            {
                public int MerchArrayCounter { get; set; }

                public string BuAccUserName { get; set; }

                public int ApplicationSource { get; set; }

                
                
                public List<Merchant> Merchants { get; set; }
            }

            public class Merchant : CreditLineObligo.Merchant
            {
                
                
                public string TrnAmt { get; set; }

                
                public string InterestPrecent { get; set; }

                
                public string InterestType { get; set; }

                
                public string ActualInterestPrecent { get; set; }

                
                public string CommisSum { get; set; }
            }
            #endregion

            #region Response
            public class Response
            {
                
                public Obligo Obligo { get; set; }
            }

            public class Obligo
            {
                
                public int MerchArrayCounter { get; set; }

                
                public float MinAmtForTrn { get; set; }

                
                public string InterestDate { get; set; }

                
                public string SRNumber { get; set; }

                
                [JsonIgnore]
                public decimal DecimalTotalTrnAmount { get; set; }

                
                [JsonIgnore]
                public decimal DecimalTotalTrnAmountDenied { get; set; }

                
                public int TotalTrnAmount { get; set; }

                
                public int TotalTrnAmountDenied { get; set; }

                
                public List<MerchantObligo> MerchantObligo { get; set; }

                public Management Management { get; set; }
            }

            public class Management
            {
                public string First_Group_Flag { get; set; }

                public string Next_Group_Flag { get; set; }

                public string Group_Number { get; set; }

                public string Group_NumOfLines { get; set; }

                public string Function { get; set; }

                public string State_ID { get; set; }

                public string First_Pacc_Flag { get; set; }

                public string Next_Pacc_Flag { get; set; }
            }

            public class MerchantObligo
            {
                
                public bool IsObligoRequestApprove { get; set; }
                
                public string ExtIdNumTypeCode { get; set; }
                
                public string ProdExtId { get; set; }
                
                public int MaxAmtForTrn { get; set; }
                
                [JsonIgnore]
                public decimal DecimalTrnAmt { get; set; }
                
                public string TrnAmt { get; set; }
                
                public string InterestPrecent { get; set; }
                
                public string InterestType { get; set; }
                
                public string InterestDesc { get; set; }
                
                public string ActualInterestPrecent { get; set; }
                
                public string CommisSum { get; set; }
                
                public string CommisDesc { get; set; }
                
                public string AccBankNum { get; set; }
                
                public string AccBranchNum { get; set; }
                
                public string AccTypeCode { get; set; }
                
                public string AccNum { get; set; }
                
                public string StatusId { get; set; }
                
                public string StatusDesc { get; set; }
            }
            #endregion
        }
    }

}
