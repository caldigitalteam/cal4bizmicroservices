using System;
using System.Collections.Generic;

using System.ComponentModel;

using Newtonsoft.Json;

namespace CreditsBpmAccessor.Models.CreditLine
{
    public partial class CreditLineEntity
    {
        //[TypeConverter(typeof(EnumNumericConverter))]
        public enum eApplicationSource
        {
            BusinessSite = 35,
            BusinessApp = 36,
            BusinessSiteOtp = 45
        }

        
        
        public class CreditLineObligo
        {
            #region Request
            
            public class Request
            {
                public int ApplicationSource { get; set; }

                public int MerchArrayCounter { get; set; }

                public string BuAccUserName { get; set; }

                
                
                public string CheckInd { get; set; }

                
                
                public List<Merchant> Merchants { get; set; }
            }

            
            public class Merchant
            {
                
                
                public int ExtIdNumTypeCode { get; set; }

                
                
                public string ProdExtId { get; set; }
            }
            #endregion

            #region Response
            
            public class Response
            {
                
                public Obligo Obligo { get; set; }
            }

            
            public class Obligo
            {
                
                public bool EnableFairCredit { get; set; }

                
                public decimal CheckedObligoSum { get; set; }

                
                public string MinAmtForTrn { get; set; }

                
                public decimal MinMerchantAmtDB { get; set; }

                
                public string CreditLineSLA { get; set; }

                
                public string InterestDate { get; set; }

                
                public List<MerchantObligo> MerchantObligo { get; set; }
            }

            
            public class MerchantObligo
            {
                
                public string ProdExtId { get; set; }

                
                public string Name { get; set; }

                
                public bool IsAllowed { get; set; }
                
                
                public decimal MerchantMaxAmtForTrn { get; set; }

                
                public string InterestPrecent { get; set; }
                
                public string InterestType { get; set; }
                
                public string InterestDesc { get; set; }                

                
                public List<MerchantBrandObligo> MerchantBrandObligo { get; set; }
            }

            
            public class MerchantBrandObligo
            {
                
                public string ExtIdNumTypeCode { get; set; }
                [JsonIgnore]
                public int ExtIdNumTypeCodeOrder { get; set; }
                
                public decimal MaxAmtForTrn { get; set; }
                
                public string InterestPrecent { get; set; }
                
                public string InterestType { get; set; }
                
                public string InterestDesc { get; set; }
                
                public string ActualInterestPrecent { get; set; }
                
                public string AdjustedInterest { get; set; }
                
                public string ActualCostRate { get; set; }
                
                public string CommisSum { get; set; }
                
                public string CommisDesc { get; set; }
                
                public string AccBankNum { get; set; }
                
                public string AccBranchNum { get; set; }
                
                public string AccTypeCode { get; set; }
                
                public string AccNum { get; set; }
                
                public string StatusId { get; set; }
                
                public string StatusDesc { get; set; }
            }
            #endregion
        }
    }

    public class CreditLineCheckInd
    {
        public const string Swipe = "0"; // ללא בדיקות וללא תיעוד   
        public const string Regular = "1"; // עם בדיקות ותיעוד 
        public const string Login = "2"; // עם בדיקות וללא תיעוד 
    }

}
