using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditsBpmAccessor.Models
{
    public partial class Bpm676CreditsDetails
    {
        public class Request
        {
            public string FromDebCrdDate { get; set; }

            public string ToDebCrdDate { get; set; }

            public string[] ARRProds { get; set; }
        }

        public class Product
        {
            public string ProdExtId { get; set; }
        }

    }
}
