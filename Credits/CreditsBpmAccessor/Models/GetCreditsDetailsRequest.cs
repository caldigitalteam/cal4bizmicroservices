
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CreditsBpmAccessor.Models
{
    public partial class GetCreditsDetails
    {
        
        public class Request
        {
           
            public string From { get; set; }

            
            public string To { get; set; }

            
            //[RegularExpression(@"^\d+$")]
            public string[] Products { get; set; }
        }
    }
}
