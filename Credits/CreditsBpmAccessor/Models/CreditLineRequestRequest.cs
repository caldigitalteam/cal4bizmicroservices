using System.Collections.Generic;
using CreditsBpmAccessor.Models.CreditLine;

namespace CreditsBpmAccessor.Models
{
    public partial class CreditLineRequest
    {
        
        public class Request
        {
            public int MerchArrayCounter { get; set; }

            public string BuAccUserName { get; set; }

            public CreditLineEntity.eApplicationSource ApplicationSource { get; set; }

           
            
            public List<Merchant> Merchants { get; set; }
        }

        public class Merchant : CreditLineObligo.Merchant
        {
           
            
            public string TrnAmt { get; set; }

           
            
            public string InterestPrecent { get; set; }

           
            
            public string InterestType { get; set; }

           
            
            public string ActualInterestPrecent { get; set; }

            
            public string CommisSum { get; set; }
        }
    }
}
