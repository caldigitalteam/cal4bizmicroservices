﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditManager.Models
{
    public class MetaDataBalance
    {
        public string CreditPreviousPeriod { get; set; }
        public string CreditNextPeriod { get; set; }
        public string DepositPreviousPeriod { get; set; }
        public string DepositNextPeriod { get; set; }
    }
}
