﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditManager.Models
{
    public class MetaDataRequest
    {
        public string CurrentVersion { get; set; }
        public string OperationSystem { get; set; }
    }
}
