﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CreditManager.Models;
using GeneralUtils;
using RESTService;

namespace CreditManager.Controllers.Credits
{
    public class MetadataManagerProxy : IMetadataManagerProxy
    {
        private readonly HttpClient _client;

        public MetadataManagerProxy(HttpClient client)
        {
            _client = client;
            _client.BaseAddress = new Uri("http://localhost:5041");
        }

        public async Task<MetaDataBalance> GetMetaDataBalanceAsync(MetaDataRequest request)
        {
            string queryString = request.QueryString();
            string path2Api = $"api/MetaData/GetMetaDataBalance?{queryString}";

            return await RestService.GetRestJsonResponse<MetaDataRequest, MetaDataBalance>(_client, path2Api, RestService.eHttpMethod.HttpGet);
        }
    }
}
