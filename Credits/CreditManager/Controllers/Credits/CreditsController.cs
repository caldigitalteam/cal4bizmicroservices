﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using CreditManager.Models;
using CreditsBpmAccessor;
using CreditsBpmAccessor.Models;
using CreditsBpmAccessor.Proxies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CreditManager.Controllers.Credits
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditsController : ControllerBase
    {
        private readonly ICreditDetailsProxy _creditDetailsProxy;
        private readonly IMetadataManagerProxy _metadataManagerProxy;

        public CreditsController(ICreditDetailsProxy creditDetailsProxy, IMetadataManagerProxy metadataManagerProxy)
        {
            _creditDetailsProxy = creditDetailsProxy;
            _metadataManagerProxy = metadataManagerProxy;
        }
        // GET api/values
        [HttpPost]
        public async Task<ActionResult<GetCreditsDetails.Response>> GetCreditDetails(GetCreditsDetails.Request request)
        {
            if (string.IsNullOrEmpty(request.From) || string.IsNullOrEmpty(request.To))
            {
                MetaDataRequest metaDataRequest = new MetaDataRequest { CurrentVersion = "", OperationSystem = "01" };
                MetaDataBalance metaDataBalance = await this._metadataManagerProxy.GetMetaDataBalanceAsync(metaDataRequest);

                DateTime from = DateTime.Now.AddMonths(int.Parse(metaDataBalance.CreditPreviousPeriod) * -1);
                DateTime to = DateTime.Now.AddMonths(int.Parse(metaDataBalance.CreditNextPeriod));

                request.From = $"{ from.Month }/{ from.Year }";
                request.To = $"{ to.Month }/{ to.Year }";
            }

            if (request.Products == null || request.Products.Length.Equals(0))
            {
                request.Products = new string[] { "4553662", "4969272" };
            }

            return Ok(await _creditDetailsProxy.GetCreditDetails(request));
        }
    }
}
