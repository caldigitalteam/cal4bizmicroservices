﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditManager.Models;

namespace CreditManager.Controllers.Credits
{
    public interface IMetadataManagerProxy
    {
        Task<MetaDataBalance> GetMetaDataBalanceAsync(MetaDataRequest request);
    }
}
