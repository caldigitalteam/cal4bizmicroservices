﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace HealthCheck
{
    public class RedisHealthcheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, string dependency, HttpClient client, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var response = await client.GetAsync(dependency);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Url not responding with 200 OK");
                }
            }
            catch (Exception)
            {
                return await Task.FromResult(HealthCheckResult.Unhealthy());
            }

            return await Task.FromResult(HealthCheckResult.Healthy());

            #region redis if anable
            //using (var redis = ConnectionMultiplexer.Connect(dependency))
            //{
            //    try
            //    {
            //        var db = redis.GetDatabase(0);
            //    }
            //    catch (Exception)
            //    {
            //        return await Task.FromResult(HealthCheckResult.Unhealthy());
            //    }
            //}
            //return await Task.FromResult(HealthCheckResult.Healthy());
            #endregion
        }
    }
}
