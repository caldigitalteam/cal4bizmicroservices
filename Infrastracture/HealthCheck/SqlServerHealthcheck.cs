﻿using System;
using System.Data.SqlClient;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace HealthCheck
{

    public class SqlServerHealthcheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, string dependency, HttpClient client, CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = new SqlConnection(dependency))
            {
                try
                {
                    await connection.OpenAsync();
                }
                catch (Exception)
                {
                    return HealthCheckResult.Unhealthy();
                }
                return HealthCheckResult.Healthy();
            }
        }
    }

}

