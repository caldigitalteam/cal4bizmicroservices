﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace HealthCheck
{
    public interface IHealthCheck
    {
        Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, string dependency, HttpClient client, CancellationToken cancellationToken = default(CancellationToken));
    }
}
