﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace RESTService
{
    public static class RestService
    {
        public static Task<RES> GetRestJsonResponse<RES>(HttpClient client, string relativePath, eHttpMethod httpMethod, string proxy = null, Dictionary<string, string> headerParams = null)
            where RES : class
        {
            return RestService.GetRestJsonResponse<object, RES>(client, relativePath, httpMethod, null, proxy, headerParams);
        }

        public static async Task<RES> GetRestJsonResponse<REQ, RES>(HttpClient client, string relativePath, eHttpMethod httpMethod, REQ data = null, string proxy = null, Dictionary<string, string> headerParams = null)
            where REQ : class
            where RES : class
        {

            try
            {
                StringContent content = null;

                if (data != null)
                {
                    //convert data to string content
                    DataContractJsonSerializer jsonSer = new DataContractJsonSerializer(typeof(REQ));
                    MemoryStream ms = new MemoryStream();
                    jsonSer.WriteObject(ms, data);
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms);
                    content = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json");
                }

                //set proxy
                var clientHandler = new HttpClientHandler();
                if (!string.IsNullOrEmpty(proxy))
                {
                    clientHandler.Proxy = new WebProxy(proxy, false, new string[] { });
                    clientHandler.UseProxy = true;
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                if (headerParams != null && headerParams.Count > 0)
                {
                    foreach (var param in headerParams)
                    {
                        client.DefaultRequestHeaders.TryAddWithoutValidation(param.Key, param.Value);
                    }
                }
                HttpResponseMessage response = null;

                switch (httpMethod)
                {
                    case eHttpMethod.HttpGet:
                        response = await client.GetAsync(relativePath);
                        break;
                    case eHttpMethod.HttpPost:
                        response = await client.PostAsync(relativePath, content);
                        break;
                    case eHttpMethod.HttpPut:
                        response = await client.PutAsync(relativePath, content);
                        break;
                    case eHttpMethod.HttpDelete:
                        response = await client.DeleteAsync(relativePath);
                        break;
                    default:
                        break;
                }

                response.EnsureSuccessStatusCode();

                string jsonResult = await response.Content.ReadAsStringAsync();
                JObject jObj = JObject.Parse(jsonResult);
                return jObj.ToObject<RES>();

            }
            catch (Exception ex)
            {
                //LogHandler.Write("GetRestJsonResponse ERROR :: URL-" + host + relativePath + ", HeaderParams-" + headerParams.ToJson() + ", BodyParams-" + data.ToJson() + ". ex:" + ex.ToJson());
                throw ex;
            }
        }



        public static Stream GetFileStream(string host, string relativePath, string proxy = null, Dictionary<string, string> headerParams = null)
        {

            try
            {
                //LogHandler.Write("GetFileStream START :: URL-" + host + relativePath + ", HeaderParams-" + headerParams.ToJson());

                //set proxy
                var clientHandler = new HttpClientHandler();
                if (!string.IsNullOrEmpty(proxy))
                {
                    clientHandler.Proxy = new WebProxy(proxy, false, new string[] { });
                    clientHandler.UseProxy = true;
                }

                using (var client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(host)
                })
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
                    if (headerParams != null && headerParams.Count > 0)
                    {
                        foreach (var param in headerParams)
                        {
                            client.DefaultRequestHeaders.TryAddWithoutValidation(param.Key, param.Value);
                        }
                    }
                    Task<Stream> response = null;

                    response = client.GetAsync(relativePath).Result.Content.ReadAsStreamAsync();

                    return response.Result;
                }
            }

            catch (Exception ex)
            {
                //LogHandler.Write("GetFileStream ERROR :: URL-" + host + relativePath + ", HeaderParams-" + headerParams.ToJson());
                throw ex;
            }
        }




        public enum eHttpMethod
        {
            HttpGet,
            HttpPost,
            HttpPut,
            HttpDelete
        }
    }
}
