﻿using System;

namespace GeneralUtils
{
    public class Cal4BizException : Exception
    {
        public Cal4BizException(int methodeId, char severityCode, int severityNumber)
        {
            MethodeId = methodeId;
            SeverityCode = severityCode;
            SeverityNumber = severityNumber;
        }

        public int MethodeId { get; }
        public char SeverityCode { get; }
        public int SeverityNumber { get; }
    }

    public class SeverityStatus
    {        
        public const char Success = 'S';        
        public const char Information = 'I';
        public const char Warning = 'W';
        public const char Error = 'E';
    }
}
