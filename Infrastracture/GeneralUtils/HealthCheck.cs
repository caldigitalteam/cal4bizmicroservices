﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GeneralUtils.HealthCheck
{
    public static class HealthCheck
    {
        public static IHealthChecksBuilder AddCalUrl(this IHealthChecksBuilder builder, string url, HealthStatus healthStatus = HealthStatus.Unhealthy)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                return builder.AddUrlGroup(new Uri(url), tags: new[] { "Readiness" }, failureStatus: healthStatus);
            }
            else
            {
                return builder;
            }
        }

        public static IHealthChecksBuilder AddCalSqlServer(this IHealthChecksBuilder builder, string connectionString, HealthStatus healthStatus = HealthStatus.Unhealthy)
        {
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                return builder.AddSqlServer(connectionString, tags: new[] { "Readiness" }, failureStatus: healthStatus);
            }
            else
            {
                return builder;
            }
        }

        public static IHealthChecksBuilder AddCalRedis(this IHealthChecksBuilder builder, string connectionString, HealthStatus healthStatus = HealthStatus.Degraded)
        {
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                return builder.AddRedis(connectionString, tags: new[] { "Readiness" }, failureStatus: healthStatus);
            }
            else
            {
                return builder;
            }
        }

        public static IApplicationBuilder UseCalHealthChecks(this IApplicationBuilder app)
        {
            app.UseHealthChecks("/health/Readiness", new HealthCheckOptions
            {
                Predicate = check => check.Tags.Contains("Readiness"),
                ResponseWriter = WriteResponse
            });
            app.UseHealthChecks("/health/liveness", new HealthCheckOptions
            {
                Predicate = check => check.Tags.Contains("Liveness"),
                ResponseWriter = WriteResponse
            });

            return app;
        }

        private static Task WriteResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";

            var json = new JObject(
                new JProperty("status", result.Status.ToString()),
                new JProperty("results", new JObject(result.Entries.Select(pair =>
                    new JProperty(pair.Key, new JObject(
                        new JProperty("status", pair.Value.Status.ToString()),
                        new JProperty("description", pair.Value.Description),
                        new JProperty("data", new JObject(pair.Value.Data.Select(
                            p => new JProperty(p.Key, p.Value))))))))));
            return httpContext.Response.WriteAsync(
                json.ToString(Formatting.Indented));
        }
    }
}
