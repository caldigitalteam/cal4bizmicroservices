﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GeneralUtils
{
    public static class ObjectExtension
    {
        public static string QueryString(this object obj)
        {
            string queryString = string.Empty;

            try
            {
                Type myType = obj.GetType();
                IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                queryString = String.Join('&', props.Select(p => $"{p.Name}={p.GetValue(obj, null)}"));
            }
            catch
            {

            }

            return queryString;
        }
    }
}
