﻿using Microsoft.Extensions.Logging;

namespace Logging
{
    public class AuditProvider : ILoggerProvider
    {
        private readonly LoggingSink loggerService;

        public AuditProvider(LoggingSink loggerService)
        {
            this.loggerService = loggerService;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new AuditLogger();
        }

        public void Dispose()
        {
        }
    }
}
