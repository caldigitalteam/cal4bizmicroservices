﻿using System.Threading.Tasks;
using LoggerService;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Logging
{
    public class LogFilterAttribute : ActionFilterAttribute
    {
        public override async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            var logContext = context.HttpContext.RequestServices.GetRequiredService<LogContext>();

            logContext.Log.ServiceName = context.Controller.ToString();

            await base.OnResultExecutionAsync(context, next);

            //_logContext.Log.Result=....
            //logger.Write...
        }
    }
}
