﻿using LoggerService;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Wrap;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Logging
{
    public class LoggingService : IHostedService
    {
        private readonly LoggingSink _loggingSink;

        public LoggingService(LoggingSink loggingSink)
        {
            _loggingSink = loggingSink;
        }

        public Task StartAsync(CancellationToken cancellationToken) => _loggingSink.StartAsync(cancellationToken);

        public Task StopAsync(CancellationToken cancellationToken) => _loggingSink.StopAsync(cancellationToken);
    }

    public class LoggingSink
    {
        private readonly Subject<LogModel> _messagesStream = new Subject<LogModel>();
        private readonly BufferBlock<IList<LogModel>> _buffer;
        private readonly ActionBlock<IList<LogModel>> _flushEntries;
        private readonly PolicyWrap _circuitBreaker;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IDisposable _link;

        public LoggingSink(IHttpContextAccessor httpContextAccessor)
        {
            var dataflowBlockOptions = new DataflowBlockOptions
            {
                BoundedCapacity = 1000
            };
            _buffer = new BufferBlock<IList<LogModel>>(dataflowBlockOptions);

            Func<IList<LogModel>, Task> action = FlushLogEntries;
            _flushEntries = new ActionBlock<IList<LogModel>>(action);

            _messagesStream
                .Buffer(TimeSpan.FromSeconds(5), 5)
                .Where(buffer => buffer.Count > 0)
                .Subscribe(_buffer.AsObserver());

            #region reference
            // See: https://github.com/App-vNext/Polly-Samples/blob/master/PollyTestClient/Samples/Demo08_Wrap-Fallback-WaitAndRetry-CircuitBreaker.cs
            #endregion
            var retryPolicy =
                Policy.Handle<Exception>()
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(retryAttempt * retryAttempt));

            _circuitBreaker = Policy.Handle<Exception>()
                .CircuitBreakerAsync(1, TimeSpan.FromSeconds(20),
                onBreak: (exception, timeout) => Stop(),
                onReset: Start)
                .WrapAsync(retryPolicy);
            _httpContextAccessor = httpContextAccessor;
        }

        private void Stop()
        {
            if (_link != null)
            {
                _link.Dispose();
            }
        }

        private void Start()
        {
            _link = _buffer.LinkTo(_flushEntries);
        }

        private LogContext CurrentLogContext => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<LogContext>();

        private Task FlushLogEntries(IList<LogModel> logEntries) =>
            _circuitBreaker.ExecuteAsync(() =>
            {
                string msg = "* " + string.Join(Environment.NewLine, logEntries);
                return Console.Out.WriteLineAsync(msg);
            });

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Stop();
            return Task.CompletedTask;
        }

        public void CreateLog(string applicationName, string serviceType, string serviceName, string className,
            string methodName, DateTime createTimeStamp, string userId, string dataFormat,
            string data, string serviceSeverity, string severityCode, string processId,
            bool isStart, string userAgent, string serverName, string tags, string ip,
            int logSeverity, string messageSchema)
        {
            LogModel log = new LogModel();
            log.ServiceType = serviceType;
            log.ServiceName = serviceName;
            log.ClassName = className;
            log.MethodName = methodName;
            log.CreateTimeStamp = createTimeStamp;
            log.UserId = userId;
            log.DataFormat = dataFormat;
            log.Data = data;
            log.ServiceSeverity = serviceSeverity;
            log.SeverityCode = severityCode;
            log.ProcessId = processId;
            log.IsStart = isStart;
            log.UserAgent = userAgent;
            log.ServerName = serverName;
            log.Tags = tags;
            log.Ip = ip;
            log.LogSeverity = LogLevel(logSeverity);
            log.MessageSchema = messageSchema;
            _messagesStream.OnNext(log);
        }

        public void CreateLog(ILogger logger)
        {
            var l = logger;

        }
        private int LogLevel(int logLeve)
        {
            // adjustment for Audit service
            return (int)logLeve - 1;
        }
    }
}
