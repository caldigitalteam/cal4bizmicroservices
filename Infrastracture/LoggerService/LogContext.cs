﻿using System;
using System.Collections.Generic;
using System.Text;
using Logging;

namespace LoggerService
{
    public class LogContext
    {
        public LogContext(string applicationName, string _processId)
        {
            Log = new LogModel { ApplicationName = applicationName, ProcessId = _processId };
        }

        public LogModel Log { get; set; }
    }
}
