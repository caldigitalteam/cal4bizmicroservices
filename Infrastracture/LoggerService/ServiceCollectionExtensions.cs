﻿using System;
using System.Collections.Generic;
using System.Text;
using Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LoggerService
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLogging(this IServiceCollection services, string applicationName)
        {
            services.AddSingleton<LoggingSink>();
            services.AddHostedService<LoggingService>();
            return services.AddScoped(serviceProvider => new LogContext(applicationName, Guid.NewGuid().ToString()));
        }
    }
}
