﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logging
{
    public class LogModel
    {
        public string ApplicationName { get; set; }
        public string ServiceType { get; set; }
        public string ServiceName { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public DateTime CreateTimeStamp { get; set; }
        public string UserId { get; set; }
        public string DataFormat { get; set; }
        public string Data { get; set; }
        public string ServiceSeverity { get; set; }
        public string SeverityCode { get; set; }
        public string ProcessId { get; set; }
        public bool IsStart { get; set; }
        public string UserAgent { get; set; }
        public string ServerName { get; set; }
        public string Tags { get; set; }
        public string Ip { get; set; }
        public int LogSeverity { get; set; }
        public string MessageSchema { get; set; }
    }

    public enum Severity : byte { Debug, Info, Warning, Error }
}
