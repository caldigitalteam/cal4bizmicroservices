using System.Threading.Tasks;

namespace RedisService
{
    public interface IRedis
    {
        Task<RES> Get<RES>(string key) where RES : class;
        Task<bool> Add<REQ>(string key, REQ value, int minuestToExpire);
        Task<bool> Delete(string key);
    }

}

