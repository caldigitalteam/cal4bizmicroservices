﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;
using System.Threading.Tasks;

namespace RedisService
{
    public class RedisClient : IRedis
    {
        private static RedisConfiguration _redisConfiguration = new RedisConfiguration();
        private NewtonsoftSerializer serializer = new NewtonsoftSerializer();
        public StackExchangeRedisCacheClient Instance { get; set; }

        public RedisClient(IConfiguration configuration)
        {
            configuration.GetSection("Redis").Bind(_redisConfiguration);
            Instance = new StackExchangeRedisCacheClient(serializer, _redisConfiguration);
        }

        Task<RES> IRedis.Get<RES>(string key)
        {
            return Instance.GetAsync<RES>(key);
        }

        public Task<bool> Add<REQ>(string key, REQ value, int minuestToExpire)
        {
            return Instance.AddAsync<REQ>(key, value, DateTimeOffset.Now.AddMinutes(minuestToExpire));
        }

        public Task<bool> Delete(string key)
        {
            return Instance.RemoveAsync(key);
        }

        
    }
}
