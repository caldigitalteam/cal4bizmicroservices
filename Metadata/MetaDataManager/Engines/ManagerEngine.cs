﻿using System;
using System.Threading.Tasks;
using DBMetadataAccessor.Engine;
using DBMetadataAccessor.Models;
using GeneralUtils;
using Newtonsoft.Json;

namespace MetaDataManager.Engines
{
    public interface IManagerEngine
    {
        Task<MetaDataResource> GetMetaData(MetaDataRequest request);

        Task<Balance> GetMetaDataBalance(MetaDataRequest request);
    }
    public class ManagerEngine : IManagerEngine
    {
        private readonly IAccessorEngine _accessorEngine;

        public ManagerEngine(IAccessorEngine accessorEngine)
        {
            _accessorEngine = accessorEngine;
        }

        public async Task<MetaDataResource> GetMetaData(MetaDataRequest request)
        {
            var response = new MetaDataResource();
            try
            {
                var t_Meta = await _accessorEngine.GetMetaDataDetailsAsync(request.OperationSystem, request.CurrentVersion);

                if (response != null)
                {
                    response = JsonConvert.DeserializeObject<MetaDataResource>(t_Meta.MetaData);
                }
                else
                {
                    throw new Cal4BizException(1, SeverityStatus.Error, 1);
                }
            }
            catch (Cal4BizException bizEx)
            {
                throw bizEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public async Task<Balance> GetMetaDataBalance(MetaDataRequest request)
        {
            var response = new Balance();
            try
            {
                var t_Meta = await _accessorEngine.GetMetaDataDetailsAsync(request.OperationSystem, request.CurrentVersion);

                if (response != null)
                {
                    response = JsonConvert.DeserializeObject<MetaDataResource>(t_Meta.MetaData).MetaData.Modules.Balance;
                }
                else
                {
                    throw new Cal4BizException(1, SeverityStatus.Error, 1);
                }
            }
            catch (Cal4BizException bizEx)
            {
                throw bizEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

    }
}
