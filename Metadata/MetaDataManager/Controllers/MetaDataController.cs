﻿using System;
using System.Threading.Tasks;
using DBMetadataAccessor.Engine;
using DBMetadataAccessor.Models;
using MetaDataManager.Engines;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MetaDataManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetaDataController : ControllerBase
    {
        private readonly IAccessorEngine _accessorEngine;
        private readonly IManagerEngine _managerEngine;

        public MetaDataController(IManagerEngine managerEngine)
        {
            _managerEngine = managerEngine;
        }

        [HttpGet]
        [Route("GetMetaData")]
        public async Task<ActionResult<MetaDataResource>> GetMetaData([FromQuery] MetaDataRequest request)
        {
            var result = await _managerEngine.GetMetaData(request);

            return result;
        }

        [HttpGet]
        [Route("GetMetaDataBalance")]
        public async Task<ActionResult<Balance>> GetMetaDataBalance([FromQuery] MetaDataRequest request)
        {
            var result = await _managerEngine.GetMetaDataBalance(request);

            return result;
        }
    }
}
