﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMetadataAccessor.Models;
using DBMetadataAccessor.Persistance;
using GeneralUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RedisService;

namespace DBMetadataAccessor.Engine
{
    public interface IAccessorEngine
    {
        Task<T_MetaData> GetMetaDataDetailsAsync(string operationSystem, string version);

    }

    public class AccessorEngine : IAccessorEngine
    {
        private readonly MetaDataDbContext _dbContext;
        private readonly IRedis _redisProxy;
        private readonly IConfiguration _configuration;

        public AccessorEngine(MetaDataDbContext dbContext, IRedis redisProxy, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _redisProxy = redisProxy;
            _configuration = configuration;
        }

        public async Task<T_MetaData> GetMetaDataDetailsAsync(string operationSystem, string version)
        {
            try
            {
                T_MetaData t_MetaData = null;

                IEnumerable<T_MetaData> metaDatas = await this._redisProxy.Get<IEnumerable<T_MetaData>>("MetaData");

                if (metaDatas == null)
                {
                    metaDatas = await _dbContext.T_MetaData.ToArrayAsync();

                    await _redisProxy.Add<IEnumerable<T_MetaData>>("MetaData", metaDatas, this._configuration.GetValue<int>("MetaDataCachTimeOut"));

                    t_MetaData = await _dbContext.T_MetaData
                        .OrderByDescending(q => new Version(q.Version))
                        .FirstOrDefaultAsync(q => q.OSCode.ToLower() == operationSystem.ToLower());
                }
                else
                {
                    t_MetaData = metaDatas
                        .OrderByDescending(q => new Version(q.Version))
                        .FirstOrDefault(q => q.OSCode.ToLower() == operationSystem.ToLower());
                }

                return t_MetaData;
            }
            catch (Cal4BizException bizex)
            {
                throw bizex;
            }
            catch (Exception ex)
            {
                throw new Cal4BizException(1, SeverityStatus.Error, 1);
            }

        }
    }    
}

