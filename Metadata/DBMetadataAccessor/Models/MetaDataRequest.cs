

namespace DBMetadataAccessor.Models
{
    public class MetaDataRequest
    {
        public string CurrentVersion { get; set; }
        public string OperationSystem { get; set; }
    }
}
