using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBMetadataAccessor.Models
{
    public class MetaDataResource
    {
        public MetaData MetaData { get; set; }
    }

    public class MetaData
    {
        public Modules Modules { get; set; }
        public CodeDescriptionPair[] Banks { get; set; }
        public CodeDescriptionPair[] TransactionTypes { get; set; }
        public StaticText[] StaticTexts { get; set; }
        public Currency[] Currencies { get; set; }
        public Navigation[] Navigation { get; set; }
        public Brand[] Brands { get; set; }
        public Support Support { get; set; }
        public Application Application { get; set; }
        public Document[] Document { get; set; }
        public AreaCode[] AreaCodes { get; set; }
    }

    public class AreaCode
    {
        public int OrderIndex { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }

    public class Document
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public string Url { get; set; }
    }

    public class Application
    {
        public string PasswordResetUrl { get; set; }
        public string RegistrationUrl { get; set; }
        public string Version { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string DownloadLink { get; set; }
        public string SessionTimeOut { get; set; }
        public string SwipeLoginType { get; set; }
        public string WebLink { get; set; }
        public Os[] Os { get; set; }
    }

    public class Os
    {
        public byte Id { get; set; }
        public object Description { get; set; }
        public bool Excluded { get; set; }
        public string Minimal { get; set; }
        public string Link { get; set; }
    }

    public class Support
    {
        public CallCenter[] CallCenter { get; set; }
    }

    public class CallCenter
    {
        public Phones Phones { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public string Hours { get; set; }
        public string Days { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }

    public class Phones
    {
        public string[] Number { get; set; }
    }

    public class Brand
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }

    public class Modules
    {
        public Cancellation Cancellation { get; set; }
        public Balance Balance { get; set; }
        public Lead Lead { get; set; }
        public CreditLine CreditLine { get; set; }
        public Calpos Calpos { get; set; }
    }

    public class Cancellation
    {
        public string HandlingPeriod { get; set; }
        public string RangeScopeLookup { get; set; }
        public string ForeignCardGracePeriod { get; set; }
        public string ManualFull { get; set; }
        public string Partial { get; set; }
        public string ManualPartial { get; set; }
        public string Full { get; set; }
    }

    public class Balance
    {
        public string CreditPreviousPeriod { get; set; }
        public string CreditNextPeriod { get; set; }
        public string DepositPreviousPeriod { get; set; }
        public string DepositNextPeriod { get; set; }
    }

    public class Lead
    {
        public string HandlingPeriod { get; set; }
        public RequestReason[] RequestReasons { get; set; }
    }

    public class RequestReason
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public bool IsLeadSubject { get; set; }
    }

    public class CreditLine
    {
        public bool QuickViewObligoInd { get; set; }
        public string CreditLineObligoAlternativeText { get; set; }
    }

    public class Calpos
    {
        public string WebLink { get; set; }
        public string ApplLink { get; set; }
        public string AppLinkAndroid { get; set; }
        public string AppLinkIOS { get; set; }
    }

    public class StaticText : CodeDescriptionPair
    {
        public string Content { get; set; }
        public string Priority { get; set; }
    }

    public class Currency : CodeDescriptionPair
    {
        public string Priority { get; set; }
        public string Symbol { get; set; }
        public string Mark { get; set; }
    }

    public class Navigation
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Dependency { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public Route Route { get; set; }
        public Authentication Authentication { get; set; }
    }

    public class Authentication
    {
        public bool Required { get; set; }
        public bool Available { get; set; }
    }

    public class Route
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public int Type { get; set; }
        public string Url { get; set; }
    }

    public class CodeDescriptionPair
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
