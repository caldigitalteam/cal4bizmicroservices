﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBMetadataAccessor.Models
{
    public class T_MetaData
    {
        [Column(Order = 0)]
        [StringLength(50)]
        public string OSCode { get; set; }

        [Required]
        [StringLength(50)]
        public string OSDesc { get; set; }
        
        [Column(Order = 0)]
        [StringLength(50)]
        public string Version { get; set; }

        [Required]
        [StringLength(500)]
        public string UpdateUrl { get; set; }
        
        public string MetaData { get; set; }

        [StringLength(50)]
        public string MaintenanceStatus { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(150)]
        public string Description { get; set; }
    }
}
