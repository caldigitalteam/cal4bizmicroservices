﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DBMetadataAccessor.Migrations
{
    public partial class InitialModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "T_MetaData",
                columns: table => new
                {
                    OSCode = table.Column<string>(maxLength: 50, nullable: false),
                    OSDesc = table.Column<string>(maxLength: 50, nullable: false),
                    Version = table.Column<string>(maxLength: 50, nullable: false),
                    UpdateUrl = table.Column<string>(maxLength: 500, nullable: false),
                    MetaData = table.Column<string>(nullable: true),
                    MaintenanceStatus = table.Column<string>(maxLength: 50, nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_MetaData", x => new { x.OSCode, x.Version });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_MetaData");
        }
    }
}
