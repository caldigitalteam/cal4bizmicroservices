﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBMetadataAccessor.Models;
using Microsoft.EntityFrameworkCore;

namespace DBMetadataAccessor.Persistance
{
    public class MetaDataDbContext : DbContext
    {
        public DbSet<T_MetaData> T_MetaData { get; set; }

        public MetaDataDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T_MetaData>().HasKey(k => new { k.OSCode, k.Version });
        }
    }
}
